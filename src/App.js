import React, {Component} from 'react'
import {View} from 'react-native'
import firebase from 'firebase'
import {Header, Button, Spinner} from "./components/common"
import LoginForm from "./components/LoginForm"


class App extends Component {

    state = {
        loggedIn: null
    }

    componentWillMount() {
        firebase.initializeApp({
            apiKey: 'AIzaSyD-Vp-JVe3jZGVHUuOSM2oYRZXMRP1Rapg',
            authDomain: 'authentication-7dea5.firebaseapp.com',
            databaseURL: 'https://authentication-7dea5.firebaseio.com',
            projectId: 'authentication-7dea5',
            storageBucket: 'authentication-7dea5.appspot.com',
            messagingSenderId: '191000714481'
        });

        // check if user loggedin or not
        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                this.setState({loggedIn: true})
            } else {
                this.setState({loggedIn: false})
            }
        });
    }

    renderContent() {

        switch (this.state.loggedIn) {
            case true:
                return (
                    <Button onPress={() => firebase.auth().signOut()}>
                        Log Out
                    </Button>
            )

            case false:
                return <LoginForm/>

            default:
                <Spinner size="large"/>
        }


        return <LoginForm/>
    }


    render() {
        return (
            <View>
                <Header headerText='Authentication'/>
                {this.renderContent()}
            </View>
        )
    }
}

export default App