import React, {Component} from 'react'
import {Card, CardSection, Button, Input, Spinner} from "./common";
import firebase from 'firebase'
import {Text} from 'react-native'


class LoginForm extends Component {

    state = {
        email: '',
        password: '',
        error: '',
        loading: false
    }


    onLoginButtonPress() {
        const {email, password} = this.state;

        this.setState({error: '', loading: true})

        firebase.auth().signInWithEmailAndPassword(email, password)
            .then(this.onLoginSuccess.bind(this))
            .catch(() => {
               firebase.auth().createUserWithEmailAndPassword(email, password)
                   .then(this.onLoginSuccess.bind(this))
                   .catch(this.onLoginFail.bind(this))
            });
    }

    onLoginFail() {
        this.setState({error: 'Authentication Failed', loading: false});
    }

    onLoginSuccess() {
        this.setState({
            email: '',
            password: '',
            loading: false
        })

    }

    renderButton() {
        if (this.state.loading) {
            return <Spinner size="small"/>
        } else {
            return (
                <Button onPress={this.onLoginButtonPress.bind(this)}>
                    Login
                </Button>
            )
        }
    }


    render() {
        return (
            <Card>
                <CardSection>
                    <Input
                        label="Email"
                        value={this.state.email}
                        onChangeText={text => this.setState({email: text})}
                        placeholder="example@gmail.com"
                    />
                </CardSection>

                <CardSection>
                    <Input
                        secureTextEntry
                        label="Password"
                        placeholder="password"
                        value={this.state.password}
                        onChangeText={password => this.setState({password})}
                    />
                </CardSection>

                <Text style={styles.errorTextMessage}>
                    {this.state.error}
                </Text>

                <CardSection>
                    {this.renderButton()}
                </CardSection>

            </Card>
        )
    }
}

const styles = {
    errorTextMessage: {
        color: 'red',
        fontSize: 20,
        alignSelf: 'center',

    }
}



export default LoginForm